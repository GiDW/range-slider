export = RangeSlider

declare class RangeSlider {
    constructor(options?: RangeSlider.IRangeSliderOptions)
    disabled: boolean
    config(options?: RangeSlider.IRangeSliderOptions): void
    setValue(value: number, executeChangeListeners?: boolean): void
    getValue(): number
    getValueHook(): ((currentValue: number, newValue: number) => RangeSlider.IRangeSliderHookResult) | null
    setValueHook(hook: ((currentValue: number, newValue: number) => RangeSlider.IRangeSliderHookResult) | null): void
    getValueFromWrapperHook(): ((currentValue: number, newValue: number) => RangeSlider.IRangeSliderHookResult) | null
    setValueFromWrapperHook(hook: ((currentValue: number, newValue: number) => RangeSlider.IRangeSliderHookResult) | null): void
    onChange(callback: (value?: number) => void): void
    onChanged(callback: (value?: number) => void): void
    offChange(callback: (value?: number) => void): void
    offChanged(callback: (value?: number) => void): void
    removeAllChangelisteners(): void
    removeAllChangedListeners(): void
    isSliding(): boolean
    detach(): void
    destroy(): void
}

declare namespace RangeSlider {
    export interface IRangeSliderOptions {
        element?: HTMLElement | string
        orientation?: 'horizontal' | 'vertical'
        reverse?: boolean
        min?: number
        max?: number
        step?: number
        start?: number
        value?: number
        progress?: boolean
        ticks?: number[]
        switchTicksPosition?: boolean
        transform3d?: boolean
        disabled?: boolean
        ignoreWhileSliding?: boolean
        setValueHook?: ((currentValue: number, newValue: number) => RangeSlider.IRangeSliderHookResult) | null
        setValueFromContainerHook?: ((currentValue: number, newValue: number) => RangeSlider.IRangeSliderHookResult) | null
        onChange?: (value?: number) => void
        onChanged?: (value?: number) => void
    }
    export interface IRangeSliderHookResult {
      value?: number
      cancelCurrentPointerEvent?: boolean
    }
}
