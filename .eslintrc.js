/* eslint-env node */
module.exports = {
  root: true,
  extends: [
    '@gidw/eslint-config-standard'
  ],
  parserOptions: {
    ecmaVersion: 5,
    sourceType: 'script'
  },
  env: {
    browser: true,
    node: false
  },
  globals: {
    Promise: 'readonly',
    require: 'readonly',
    module: 'readonly',
    exports: 'readonly'
  },
  rules: {
    'dot-notation': 'off',
    'max-len': [
      'error',
      {
        code: 80,
        ignoreUrls: true,
        ignoreStrings: true,
        ignoreTemplateLiterals: true,
        ignoreRegExpLiterals: true
      }
    ],
    'padded-blocks': 'off',
    'prefer-promise-reject-errors': 'off',
    'vars-on-top': 'error',
    'function-paren-newline': ['error', 'multiline-arguments'],
    'function-call-argument-newline': ['error', 'consistent'],
    'space-before-function-paren': ['error', 'always'],
    'no-unused-vars': [
      'error',
      {
        args: 'all',
        argsIgnorePattern: '^_',
        caughtErrors: 'none',
        ignoreRestSiblings: true,
        vars: 'all'
      }
    ],
    'no-param-reassign': 'error',
    'no-shadow': 'error',
    'capitalized-comments': [
      'error',
      'always',
      {
        ignoreConsecutiveComments: true,
        ignorePattern: 'region|endregion|iOS|noinspection|iTunes'
      }
    ],
    'no-console': 'error'
  },
  ignorePatterns: [
    'node_modules/',
    '**/*.min.js',
    '**/*.d.ts',
    'build/',
    'dist/',
    'public/'
  ]
}
