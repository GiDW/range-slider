/* eslint-disable dot-notation */
(function (root, factory) {
  // eslint-disable-next-line no-undef
  if (typeof define === 'function' && define.amd) {
    // AMD
    // eslint-disable-next-line no-undef
    define([], factory)

    // eslint-disable-next-line no-undef
  } else if (typeof module === 'object' && module.exports) {
    // Node
    // eslint-disable-next-line no-undef
    module.exports = factory()
  } else {
    if (!root['gidw']) root['gidw'] = {}
    // Browser globals
    root['gidw']['RangeSlider'] = factory()
  }
// eslint-disable-next-line no-undef
}(typeof self !== 'undefined' ? self : this, function () {
  'use strict'

  var _activeListenerOptions = {
    capture: false,
    passive: false
  }

  var _passiveListenerOptions = {
    capture: false,
    passive: true
  }

  /**
   * @param {TouchEvent} event
   * @param {number} id
   * @returns {?Touch}
   */
  function getTouch (event, id) {
    var touches, length, i
    touches = event.changedTouches
    length = touches.length
    for (i = 0; i < length; i++) {
      if (touches[i].identifier === id) return touches[i]
    }
    return null
  }

  /**
   * @constructor
   * @param {Object} [options]
   */
  function RangeSlider (options) {
    this._thumbTouchStartHandler = this._onThumbTouchStart.bind(this)
    this._thumbMouseDownHandler = this._onThumbMouseDown.bind(this)

    this._containerTouchStartHandler = this._onWrapperTouchStart.bind(this)
    this._containerMouseDownHandler = this._onWrapperMouseDown.bind(this)

    this._moveHandler = this._onMove.bind(this)
    this._endHandler = this._onEnd.bind(this)

    this._elWrapper = document.createElement('div')
    this._elContainer = document.createElement('div')
    this._elTrack = document.createElement('div')
    this._elTicksContainer = document.createElement('div')
    this._elProgress = document.createElement('div')
    this._elThumbContainer = document.createElement('div')
    this._elThumb = document.createElement('div')

    this._elWrapper.classList.add('grsw')
    this._elContainer.classList.add('grsc')
    this._elTrack.classList.add('grstr')
    this._elTicksContainer.classList.add('grstc')
    this._elProgress.classList.add('grspr')
    this._elThumbContainer.classList.add('grsthc')
    this._elThumb.classList.add('grsth')

    this._elContainer.appendChild(this._elTrack)
    this._elThumbContainer.appendChild(this._elThumb)
    this._elContainer.appendChild(this._elThumbContainer)
    this._elWrapper.appendChild(this._elContainer)

    this._parentElement = null

    // Event variables

    this._touchId = undefined
    this._width = 0
    this._height = 0
    this._xMin = 0
    this._yMin = 0
    this._xMax = 0
    this._yMax = 0
    this._lastEventEndTimestamp = 0
    this._isSliding = false

    // Properties

    this._isVertical = false
    this._reverse = false
    this._min = 0
    this._max = 100
    this._step = 0.1
    this._sliderValue = 0.5
    this._progress = false
    this._start = this._min
    this._disabled = false
    this._ignoreWhileSliding = false
    this._ticks = []
    this._ticksLength = 0
    this._hasTicks = false
    this._switchTicksPosition = false
    this._transform3d = false

    this._setValueHook = null
    this._setValueFromWrapperHook = null

    this._onChangeListeners = []
    this._onChangedListeners = []

    this['config'](options)

    this._canceled = false

    if (this._parentElement) {
      while (this._parentElement.firstChild) {
        this._parentElement.removeChild(this._parentElement.firstChild)
      }
    }

    this._attachToParent()
  }

  Object.defineProperty(RangeSlider.prototype, 'disabled', {
    get: function () {
      return this._disabled
    },
    set: function (value) {
      this._disabled = !!value
      this._processDisabled()
    }
  })

  /**
   * Parse RangeSlider options object
   *
   * @param {Object} [options]
   */
  RangeSlider.prototype['config'] = function (options) {
    var result, arr, length, i, entry
    if (typeof options === 'object' && options) {
      // Element
      if (typeof options['element'] === 'string') {
        this._parentElement = document.getElementById(options['element'])
      } else if (options['element'] &&
        options['element'].contains &&
        options['element'].appendChild) {
        this._parentElement = options['element']
      }
      // Properties
      if (options['orientation'] === 'horizontal' ||
          options['orientation'] === 'vertical') {
        this._isVertical = options['orientation'] === 'vertical'
      }
      if (typeof options['reverse'] === 'boolean') {
        this._reverse = options['reverse']
      }
      if (typeof options['min'] === 'number' && isFinite(options['min'])) {
        this._min = options['min']
      }
      if (typeof options['max'] === 'number' && isFinite(options['max'])) {
        this._max = options['max']
      }
      if (typeof options['step'] === 'number' && isFinite(options['step'])) {
        this._step = options['step']
      }
      if (typeof options['progress'] === 'boolean') {
        this._progress = options['progress']
      }
      if (typeof options['start'] === 'number' && isFinite(options['start'])) {
        this._start = options['start']
      }
      arr = options['ticks']
      if (Array.isArray(arr)) {
        result = []
        length = arr.length
        for (i = 0; i < length; i++) {
          entry = arr[i]
          // Check tick entries
          if (typeof entry === 'number' &&
              entry >= this._min &&
              entry <= this._max) {
            result.push(entry)
          }
        }
        this._ticks = result
        this._ticksLength = this._ticks.length
        this._hasTicks = this._ticksLength > 0
      }
      if (typeof options['switchTicksPosition'] === 'boolean') {
        this._switchTicksPosition = options['switchTicksPosition']
      }
      if (typeof options['transform3d'] === 'boolean') {
        this._transform3d = options['transform3d']
      }
      if (typeof options['disabled'] === 'boolean') {
        this._disabled = options['disabled']
      }
      if (typeof options['ignoreWhileSliding'] === 'boolean') {
        this._ignoreWhileSliding = options['ignoreWhileSliding']
      }
      if (typeof options['setValueHook'] === 'function') {
        this['setValueHook'](options['setValueHook'])
      }
      if (typeof options['setValueFromWrapperHook'] === 'function') {
        this['setValueFromWrapperHook'](options['setValueFromWrapperHook'])
      }
      if (typeof options['onChange'] === 'function') {
        this['onChange'](options['onChange'])
      }
      if (typeof options['onChanged'] === 'function') {
        this['onChanged'](options['onChanged'])
      }
      if (typeof options['value'] === 'number' &&
          isFinite(options['value'])) {
        // Prepare for normalize
        this._range = this._max - this._min
        this._sliderValue = this._normalize(options['value'])
      }
    }
    this._processOptions()
  }

  RangeSlider.prototype['getValueHook'] = function () {
    return this._setValueHook
  }

  RangeSlider.prototype['setValueHook'] = function (hook) {
    if (typeof hook === 'function') {
      this._setValueHook = hook
    }
  }

  RangeSlider.prototype['getValueFromWrapperHook'] = function () {
    return this._setValueFromWrapperHook
  }

  RangeSlider.prototype['setValueFromWrapperHook'] = function (hook) {
    if (typeof hook === 'function') {
      this._setValueFromWrapperHook = hook
    }
  }

  RangeSlider.prototype['onChange'] = function (callback) {
    if (typeof callback === 'function') {
      this._onChangeListeners.push(callback)
    }
  }

  RangeSlider.prototype['onChanged'] = function (callback) {
    if (typeof callback === 'function') {
      this._onChangedListeners.push(callback)
    }
  }

  RangeSlider.prototype['offChange'] = function (callback) {
    var length, i
    length = this._onChangeListeners.length
    for (i = 0; i < length; i++) {
      if (this._onChangeListeners[i] === callback) {
        this._onChangeListeners.splice(i, 1)
      }
    }
  }

  RangeSlider.prototype['offChanged'] = function (callback) {
    var length, i
    length = this._onChangedListeners.length
    for (i = 0; i < length; i++) {
      if (this._onChangedListeners[i] === callback) {
        this._onChangedListeners.splice(i, 1)
      }
    }
  }

  RangeSlider.prototype['removeAllChangelisteners'] = function () {
    this._onChangeListeners = []
  }

  RangeSlider.prototype['removeAllChangedListeners'] = function () {
    this._onChangedListeners = []
  }

  RangeSlider.prototype._executeChangeListeners = function (value) {
    var length, i
    length = this._onChangeListeners.length
    for (i = 0; i < length; i++) this._onChangeListeners[i](value)
  }

  RangeSlider.prototype._executeChangedListeners = function (value) {
    var length, i
    length = this._onChangedListeners.length
    for (i = 0; i < length; i++) this._onChangedListeners[i](value)
  }

  RangeSlider.prototype._processOptions = function () {
    if (this._progress) {
      if (!this._elTrack.contains(this._elProgress)) {
        this._elTrack.appendChild(this._elProgress)
      }
    } else {
      if (this._elTrack.contains(this._elProgress)) {
        this._elTrack.removeChild(this._elProgress)
      }
    }
    if (this._isVertical) {
      this._elWrapper.classList.remove('grshor')
      this._elWrapper.classList.add('grsver')
    } else {
      this._elWrapper.classList.remove('grsver')
      this._elWrapper.classList.add('grshor')
    }
    this._processDisabled()
    this._range = this._max - this._min
    this._normalizedStep = this._step / this._range
    this._normalizedStart = this._normalize(this._start)
    this._setStart()
    this._processTicks()
    this._syncUi()
  }

  RangeSlider.prototype._processDisabled = function () {
    if (this._disabled) {
      this._removeStartListeners()
      this._endAction()
      this._elWrapper.classList.add('grs-dis')
    } else {
      this._elWrapper.classList.remove('grs-dis')
      this._setStartListeners()
    }
  }

  RangeSlider.prototype._processTicks = function () {
    var ticks, i, length, tickValue, percent
    var tickContainerElement, tickLineElement, tickValueElement
    var valueText, docFragment

    // Clear old ticks
    while (this._elTicksContainer.firstChild) {
      this._elTicksContainer.removeChild(this._elTicksContainer.firstChild)
    }

    if (this._hasTicks) {
      if (!this._elContainer.contains(this._elTicksContainer)) {
        this._elContainer.insertBefore(this._elTicksContainer, this._elTrack)
      }
    } else {
      if (this._elContainer.contains(this._elTicksContainer)) {
        this._elContainer.removeChild(this._elTicksContainer)
      }
    }

    this._elWrapper.classList.toggle('grs--has-ticks', this._hasTicks)

    this._elWrapper.classList.toggle('grs--tpn', !this._switchTicksPosition)
    this._elWrapper.classList.toggle('grs--tps', this._switchTicksPosition)

    if (this._hasTicks) {
      docFragment = document.createDocumentFragment()

      ticks = this._ticks
      length = this._ticksLength
      for (i = 0; i < length; i++) {
        tickValue = ticks[i]
        percent = this._normalize(tickValue) * 100
        if (this._reverse) percent = Math.abs(percent - 100)

        tickContainerElement = document.createElement('div')
        tickValueElement = document.createElement('div')
        tickLineElement = document.createElement('div')
        valueText = document.createTextNode('' + tickValue)

        tickContainerElement.classList.add('grstt')
        tickValueElement.classList.add('grsttv')
        tickLineElement.classList.add('grsttl')

        if (this._isVertical) {
          tickContainerElement.style.top = percent + '%'
        } else {
          tickContainerElement.style.left = percent + '%'
        }

        if (tickValue === this._min) {
          tickContainerElement.classList.add(
            this._reverse ? 'grstt--last' : 'grstt--first'
          )
        } else if (tickValue === this._max) {
          tickContainerElement.classList.add(
            this._reverse ? 'grstt--first' : 'grstt--last'
          )
        }

        tickValueElement.appendChild(valueText)
        tickContainerElement.appendChild(tickValueElement)
        tickContainerElement.appendChild(tickLineElement)

        docFragment.appendChild(tickContainerElement)
      }

      this._elTicksContainer.appendChild(docFragment)
    }
  }

  RangeSlider.prototype._normalize = function (value) {
    return (value - this._min) / this._range
  }

  RangeSlider.prototype._denormalize = function (value) {
    return value * this._range + this._min
  }

  RangeSlider.prototype._setStart = function () {
    var percent
    percent = this._normalizedStart * 100
    if (this._reverse) percent = Math.abs(percent - 100)
    if (this._progress) {
      if (this._isVertical) {
        this._elProgress.style.top = percent + '%'
      } else {
        this._elProgress.style.left = percent + '%'
      }
    }
  }

  RangeSlider.prototype._normalizedRoundToStep = function (normalizedValue) {
    return Math.round(normalizedValue / this._normalizedStep) *
      this._normalizedStep
  }

  RangeSlider.prototype._roundToStep = function (value) {
    return Math.round(value / this._step) * this._step
  }

  RangeSlider.prototype._syncUi = function () {
    var value
    value = this._sliderValue * 100
    if (this._reverse) value = Math.abs(value - 100)
    this._elThumbContainer.style.transform = this._transform3d
      // 3D transform
      ? this._isVertical
        ? 'translate3d(0,' + value + '%,0)'
        : 'translate3d(' + value + '%,0,0)'
      // Normal transform
      : this._isVertical
        ? 'translateY(' + value + '%)'
        : 'translateX(' + value + '%)'
    if (this._elProgress) {
      value = this._sliderValue - this._normalizedStart
      if (this._reverse) value *= -1
      this._elProgress.style.transform = this._transform3d
        // 3D transform
        ? this._isVertical
          ? 'scale3d(1,' + value + ',1)'
          : 'scale3d(' + value + ',1,1)'
        // Normal transform
        : this._isVertical
          ? 'scaleY(' + value + ')'
          : 'scaleX(' + value + ')'
    }
  }

  /**
   * Sets the slider value and executes change listeners according to parameter
   *
   * @param {number} value
   * @param {boolean} [executeChangeListeners = true]
   */
  RangeSlider.prototype['setValue'] = function (value, executeChangeListeners) {

    if (
      this._ignoreWhileSliding &&
      this._isSliding
    ) {

      // Slider is active, return
      return
    }

    this._setValue(value, executeChangeListeners)
  }

  /**
   * Sets the slider value and executes change listeners according to parameter
   *
   * @private
   * @param {number} value
   * @param {boolean} [executeChangeListeners = true]
   */
  RangeSlider.prototype._setValue = function (value, executeChangeListeners) {
    var _executeChangeListeners
    _executeChangeListeners = true

    if (typeof executeChangeListeners === 'boolean') {
      _executeChangeListeners = executeChangeListeners
    }

    if (typeof value === 'number' &&
        isFinite(value) &&
        this['getValue']() !== value) {
      this._sliderValue = this._normalizedRoundToStep(this._normalize(value))
      this._syncUi()

      if (_executeChangeListeners) {
        this._executeChangeListeners(this['getValue']())
      }
    }
  }

  RangeSlider.prototype['getValue'] = function () {
    return this._roundToStep(this._denormalize(this._sliderValue))
  }

  /**
   * Appends the slider to the parent element
   *
   * @private
   */
  RangeSlider.prototype._attachToParent = function () {
    if (this._parentElement &&
        !this._parentElement.contains(this._elWrapper)) {
      this._parentElement.appendChild(this._elWrapper)
    }
  }

  RangeSlider.prototype._removeFromParent = function () {
    if (this._parentElement &&
        this._parentElement.contains(this._elWrapper)) {
      this._parentElement.removeChild(this._elWrapper)
    }
  }

  /**
   * @param {TouchEvent} event
   */
  RangeSlider.prototype._onThumbTouchStart = function (event) {
    this._touchStart(this._elThumb, event, false)
  }

  /**
   * @param {TouchEvent} event
   */
  RangeSlider.prototype._onWrapperTouchStart = function (event) {
    this._touchStart(this._elWrapper, event, true)
  }

  /**
   * @param {HTMLElement} element
   * @param {TouchEvent} event
   * @param {boolean} initiatedFromContainer
   */
  RangeSlider.prototype._touchStart = function (
    element,
    event,
    initiatedFromContainer
  ) {
    var touch
    if (this._touchId === undefined &&
        event.changedTouches &&
        event.changedTouches.length === 1) {
      touch = event.changedTouches[0]
      this._touchId = touch.identifier
      this._setTouchListeners(element)
      this._onStart(event, initiatedFromContainer, touch)
    }
  }

  /**
   * @param {MouseEvent} event
   */
  RangeSlider.prototype._onThumbMouseDown = function (event) {
    this._setGlobalMouseListeners()
    this._onStart(event, false)
  }

  /**
   * @param {MouseEvent} event
   */
  RangeSlider.prototype._onWrapperMouseDown = function (event) {
    // If timestamp of this event is the same as the timestamp of the last end
    //  event, this mousedown is a faulty event that shouldn't have been fired
    if (event.timeStamp === this._lastEventEndTimestamp) return
    this._setGlobalMouseListeners()
    this._onStart(event, true)
  }

  /**
   * @param {(MouseEvent|TouchEvent)} event
   * @param {boolean} initiatedFromContainer
   * @param {?Touch} [touch]
   */
  RangeSlider.prototype._onStart = function (
    event,
    initiatedFromContainer,
    touch
  ) {
    var rect, windowScrollX, windowScrollY

    event.stopImmediatePropagation()
    if (event.cancelable) event.preventDefault()

    this._elWrapper.classList.add('grs-a')
    this._isSliding = true

    windowScrollX = window.scrollX
    windowScrollY = window.scrollY
    rect = this._elTrack.getBoundingClientRect()
    this._width = rect.width
    this._height = rect.height
    this._xMin = rect.left + windowScrollX
    this._yMin = rect.top + windowScrollY
    this._xMax = this._xMin + this._width
    this._yMax = this._yMin + this._height

    this._moveThumb(event, touch, initiatedFromContainer)
  }

  /**
   * @param {(MouseEvent|TouchEvent)} event
   */
  RangeSlider.prototype._onMove = function (event) {
    var touch, executeMove
    executeMove = false
    if (typeof this._touchId === 'number') {
      touch = getTouch(event, this._touchId)
      if (touch) executeMove = true
    } else {
      executeMove = true
    }
    if (executeMove) {
      event.stopImmediatePropagation()
      if (event.cancelable) event.preventDefault()
      this._moveThumb(event, touch, false)
    }
  }

  /**
   * @param {(MouseEvent|TouchEvent)} event
   */
  RangeSlider.prototype._onEnd = function (event) {
    var touch
    this._lastEventEndTimestamp = event.timeStamp
    if (typeof this._touchId === 'number') {
      if (event.changedTouches) {
        touch = getTouch(event, this._touchId)
        if (touch) this._end()
      }
    } else {
      this._end()
    }
  }

  RangeSlider.prototype._endAction = function () {
    this._touchId = undefined
    this._removeTouchListeners()
    this._removeGlobalMouseListeners()
    this._elWrapper.classList.remove('grs-a')
    this._isSliding = false
    this._canceled = false
  }

  RangeSlider.prototype._end = function () {
    this._endAction()
    this._executeChangedListeners(this['getValue']())
  }

  /**
   * @private
   * @param {(MouseEvent|TouchEvent)} event
   * @param {?Touch} [touch]
   * @param {boolean} [initiatedFromContainer]
   */
  RangeSlider.prototype._moveThumb = function (
    event,
    touch,
    initiatedFromContainer
  ) {
    var point, value, hookReturn
    var originalValue, newValue, finalValue

    if (this._canceled) return

    originalValue = this['getValue']()

    if (this._isVertical) {
      point = touch ? touch.pageY : event.pageY
      if (point > this._yMax) {
        newValue = this._reverse ? this._min : this._max
      } else if (point < this._yMin) {
        newValue = this._reverse ? this._max : this._min
      } else {
        value = (point - this._yMin) / this._height
        if (this._reverse) value = Math.abs(value - 1)
        newValue = this._roundToStep(this._denormalize(value))
      }
    } else {
      point = touch ? touch.pageX : event.pageX
      if (point > this._xMax) {
        newValue = this._reverse ? this._min : this._max
      } else if (point < this._xMin) {
        newValue = this._reverse ? this._max : this._min
      } else {
        value = (point - this._xMin) / this._width
        if (this._reverse) value = Math.abs(value - 1)
        newValue = this._roundToStep(this._denormalize(value))
      }
    }

    // If value remains the same we shouldn't do anything
    if (newValue === originalValue) return

    finalValue = newValue

    // If initiated from the container:
    //  Execute setValueFromWrapperHook (if given)
    if (initiatedFromContainer &&
        typeof this._setValueFromWrapperHook === 'function') {
      hookReturn = this._setValueFromWrapperHook(originalValue, newValue)

      // Parse hook return object
      parseHookReturn.call(this)
    }

    // Always execute general setValueHook (if given)
    if (typeof this._setValueHook === 'function') {
      hookReturn = this._setValueHook(originalValue, finalValue)

      // Parse hook return object
      parseHookReturn.call(this)
    }

    // Pass the final value to the 'setValue' method
    this._setValue(finalValue, true)

    function parseHookReturn () {
      var _value
      if (typeof hookReturn === 'object' && hookReturn !== null) {
        _value = hookReturn['value']
        if (typeof _value === 'number' &&
            isFinite(_value) &&
            _value >= this._min &&
            _value <= this._max) {
          finalValue = _value
        }

        if (typeof hookReturn['cancelCurrentPointerEvent'] === 'boolean') {
          this._canceled = hookReturn['cancelCurrentPointerEvent']
        }
      }
    }
  }

  RangeSlider.prototype._setStartListeners = function () {
    this._elThumb.addEventListener(
      'touchstart',
      this._thumbTouchStartHandler,
      _activeListenerOptions
    )
    this._elWrapper.addEventListener(
      'touchstart',
      this._containerTouchStartHandler,
      _activeListenerOptions
    )
    this._elThumb.addEventListener(
      'mousedown',
      this._thumbMouseDownHandler,
      _activeListenerOptions
    )
    this._elWrapper.addEventListener(
      'mousedown',
      this._containerMouseDownHandler,
      _activeListenerOptions
    )
  }

  RangeSlider.prototype._removeStartListeners = function () {
    this._elThumb.removeEventListener(
      'touchstart',
      this._thumbTouchStartHandler,
      _activeListenerOptions
    )
    this._elWrapper.removeEventListener(
      'touchstart',
      this._containerTouchStartHandler,
      _activeListenerOptions
    )
    this._elThumb.removeEventListener(
      'mousedown',
      this._thumbMouseDownHandler,
      _activeListenerOptions
    )
    this._elWrapper.removeEventListener(
      'mousedown',
      this._containerMouseDownHandler,
      _activeListenerOptions
    )
  }

  /**
   * Sets the global touch event listeners
   *
   * @private
   * @param {HTMLElement} element
   */
  RangeSlider.prototype._setTouchListeners = function (element) {
    element.addEventListener(
      'touchend',
      this._endHandler,
      _passiveListenerOptions
    )
    element.addEventListener(
      'touchcancel',
      this._endHandler,
      _passiveListenerOptions
    )
    element.addEventListener(
      'touchmove',
      this._moveHandler,
      _activeListenerOptions
    )
  }

  /**
   * Sets the golbal mouse event listeners
   *
   * @private
   */
  RangeSlider.prototype._setGlobalMouseListeners = function () {
    window.addEventListener(
      'mouseup',
      this._endHandler,
      _passiveListenerOptions
    )
    window.addEventListener(
      'mousemove',
      this._moveHandler,
      _activeListenerOptions
    )
  }

  /**
   * Remove the global touch listeners
   *
   * @private
   */
  RangeSlider.prototype._removeTouchListeners = function () {
    this._elThumb.removeEventListener(
      'touchmove',
      this._moveHandler,
      _activeListenerOptions
    )
    this._elThumb.removeEventListener(
      'touchcancel',
      this._endHandler,
      _passiveListenerOptions
    )
    this._elThumb.removeEventListener(
      'touchend',
      this._endHandler,
      _passiveListenerOptions
    )
    this._elWrapper.removeEventListener(
      'touchmove',
      this._moveHandler,
      _activeListenerOptions
    )
    this._elWrapper.removeEventListener(
      'touchcancel',
      this._endHandler,
      _passiveListenerOptions
    )
    this._elWrapper.removeEventListener(
      'touchend',
      this._endHandler,
      _passiveListenerOptions
    )
  }

  /**
   * Remove the global mouse listeners
   *
   * @private
   */
  RangeSlider.prototype._removeGlobalMouseListeners = function () {
    window.removeEventListener(
      'mousemove',
      this._moveHandler,
      _activeListenerOptions
    )
    window.removeEventListener(
      'mouseup',
      this._endHandler,
      _passiveListenerOptions
    )
  }

  RangeSlider.prototype['isSliding'] = function () {
    return this._isSliding
  }

  RangeSlider.prototype['detach'] = function () {
    this._removeStartListeners()
    this._endAction()
    this['removeAllChangelisteners']()
    this['removeAllChangedListeners']()
  }

  RangeSlider.prototype['destroy'] = function () {
    this['detach']()
    this._removeFromParent()
    this._parentElement = null
    this._setValueHook = null
    this._setValueFromWrapperHook = null
  }

  return RangeSlider
}))
